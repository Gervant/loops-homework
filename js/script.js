//Циклы нужны для прохода по частям программы определенное кол-во раз без повторения кода написанного вручную,
//а также являются проверкой условия для выполнения различных частей кода.
let number = null;
do {
    number = +prompt("Type your number")
} while (!number || !Number.isInteger(number))
if (number < 5){
    console.log("Sorry,no numbers")
}
else {
    for (let i = 0; i <= number; i += 5){
        console.log(i)
    }
}
let m =null;
let n = null;
do {
    m = +prompt('Enter first number');
    n = +prompt('Enter second number');
    if (m >= n){
        alert("No primes in the given range")
    }
} while (m >= n || !Number.isInteger(m) || !Number.isInteger(n)|| !m || !n)
for (let i = m; i <= n; i++) {
    if (i == 2 || i == 3 || i == 5 || i == 7 || i % 2 !== 0 && i % 3 !== 0 && i % 5 !== 0 && i % 7 !== 0) {
        console.log(i)
    }
}
